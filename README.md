# sehnsucht

#### 项目介绍
基于cron表达式的定时任务调度，python版spring scheduler

#### 软件架构
软件架构说明
1. cron
2. scheduler task

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明
cron expression说明 （年 月 日 时 分 秒）
1. 年 最小值：1000，最大值：9999 可用符号：*,-/
2. 月 最小值：1，最大值：12 可用符号：*,-/
3. 日 最小值：1，最大值：31（基于不同的年份和月份有所改变，如闰年2月最大天数为29） 可用符号：*,-/
4. 时 最小值：0，最大值：23 可用符号：*,-/
5. 分 最小值：0，最大值：59 可用符号：*,-/
6. 秒 最小值：0，最大值：59 可用符号：*,-/

/ 表示从某个时刻开始，每增加一次的数值，如2018/3 表示从2018年开始每隔3年<br>
\* 表示任意时刻开始，如果后续有/递增符号，表示每增加一次的数值，没有则默认为1，如* * * * * */5，表示每5秒<br>
, 表示分隔多个值，如2018，2021，2022，表示2018年，2021年，2022年<br>
\- 表示多个值的范围，如2018-2022，表示2018年至2022年，即2018，2019，2020，2021，2022<br>

举例说明<br>
\* * * * * */5 每隔5秒<br>
\* * * * 15-40 5/10 每小时的15分钟至40分钟内从第5秒开始，每隔10秒增加一次

scheduler task说明，在需要启动定时任务的方法上增加方法注解 @scheduler(cron, retry_times, retry_interval)
1. 参数cron即cron expression
2. retry_times 如果执行过程产生异常，重试的次数
3. retry_interval 如果执行过程产生异常，重试执行时间的时间间隔，单位为秒

举例说明

<pre><code>
@scheduler(cron='* * * * * 1,10,30,40', retry_times=5, retry_interval=3)
def task1():
    print("task 1")
</code></pre>

<pre><code>
@scheduler(cron='* * * * 15-40 5/10', retry_times=3, retry_interval=10)
def task2():
    print("task 2")
</code></pre>

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)